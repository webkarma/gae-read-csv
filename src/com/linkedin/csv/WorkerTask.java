package com.linkedin.csv;

import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.opencsv.CSVReader;

public class WorkerTask extends HttpServlet {

	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String key = request.getParameter("blobKey");

		BlobKey blobKey = new BlobKey(key);

		BlobstoreInputStream is = new BlobstoreInputStream(blobKey);

		CSVReader csvReader = new CSVReader(new InputStreamReader(is));
		String[] row = null;
		
		while ((row = csvReader.readNext()) != null) {

			Key CSVEntryKey = KeyFactory.createKey("CSVEntry", "arsenicraghav");

			Entity CSVEntry = new Entity("CSVEntry", CSVEntryKey);
			
			CSVEntry.setProperty("csv_cell_content", row[0]);

			datastore.put(CSVEntry);
		}

		csvReader.close();

	}

}
