package com.linkedin.csv;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

@SuppressWarnings("serial")
public class LoadCSV extends HttpServlet {

	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String blobKey = request.getParameter("blob-key");

		// Add the task to the default queue.
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/worker").param("blobKey", blobKey));

		response.setContentType("text/plain");

		response.getWriter()
		.println("OK : CSV successfully uploaded and task queue started! Pls check datastore after some time.");

	}
}
